

def count_men():
    with open('example_text.txt', 'r') as file:
        text = file.read()
    text = text.lower()
    num_men = text.count(' men ')
    if text[0:3] == 'men':
        num_men += 1
    return num_men


def capitalize_women():
    with open('example_text.txt', 'r') as file:
        text = file.read()
    new_women = text.replace('women', 'WOMEN')
    new_women = new_women.replace('Women', 'WOMEN')

    with open('example_text_new.txt', 'w') as newfile:
        newfile.write(new_women)


def contains_blue_devil():
    with open('example_text.txt', 'r') as file:
        text = file.read()
    return 'Blue Devil' in text


def find_non_terminal_said():
    pass


def cap_multi_vowel_words():
    pass


if __name__ == '__main__':
    with open('example_text.txt', 'r') as file:
        print('Readline():')
        print(file.readline())
        print('\nReadlines():')
        print(file.readlines())

    print(count_men())
